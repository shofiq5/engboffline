# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "GB Offline GSA",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("GB Offline GSA")
		}
	]
